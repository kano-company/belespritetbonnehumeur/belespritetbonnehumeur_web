<?php
	header('Content-Type: application/json');

	$quotations = [];
	$handle = fopen('../../../../data/citations.csv','r');
	while (($data = fgetcsv($handle)) !== FALSE) {
		$quotations[] = $data;
	}

	$quotation = $quotations[array_rand($quotations)];

	echo json_encode(
		array(
			"quotation" => $quotation[2],
			"author" => empty($quotation[0]) ? null : $quotation[0],
			"work" => empty($quotation[1]) ? null : $quotation[1],
			"url" => empty($quotation[3]) ? null : $quotation[3]
		),
		JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
	);
?>