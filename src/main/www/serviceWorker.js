var cacheName = 'belespritetbonnehumeur_v@version@';
var appFiles = [
	'/',
	'/script.js',
	'/style.css',
	'/images/belespritetbonnehumeur.svg',
	'/images/ic_share.svg'
];

self.addEventListener('install', (e) => {
	e.waitUntil(
		caches.open(cacheName).then((cache) => cache.addAll(appFiles))
	);
});

self.addEventListener('fetch', (e) => {
	e.respondWith(
		caches.match(e.request).then((response) => response || fetch(e.request)),
	);
});

self.addEventListener('activate', (e) => {
	e.waitUntil(
		caches.keys().then((keyList) => {
			return Promise.all(keyList.map((key) => {
				if (key !== cacheName) {
					return caches.delete(key);
				}
			}));
		})
	);
});