const KEY_NEXT_TIME = "next_time";
const KEY_QUOTATION = "quotation";

if('serviceWorker' in navigator) {
	navigator.serviceWorker.register('serviceWorker.js');
};

window.addEventListener("DOMContentLoaded", function (event) {
	if (navigator.share) {
		document.getElementById("share").addEventListener("click", share);
	} else {
		document.getElementById("share").style.display = "none";
	}

	loadQuotation();
});

function loadQuotation() {
	let nextTime = localStorage.getItem(KEY_NEXT_TIME);
	let now = Date.now();

	if (nextTime < now) {
		fetch("/api/quotation/random/")
			.then(function(response) {
				if(response.ok) {
					response.json().then(function(quotation) {
						displayQuotation(quotation);
						localStorage.setItem(KEY_QUOTATION, JSON.stringify(quotation));
						let delay = Math.floor((Math.sqrt(Math.random())*18+6)*3600000);
						localStorage.setItem(KEY_NEXT_TIME, now + delay);
						setTimeout(loadQuotation, delay);
					});
				} else {
					displayQuotation(JSON.parse(localStorage.getItem(KEY_QUOTATION)));
				}
			});
	} else {
		displayQuotation(JSON.parse(localStorage.getItem(KEY_QUOTATION)));
		setTimeout(loadQuotation, nextTime-now);
	}
}

function displayQuotation(quotation) {
	document.getElementById("quotation").getElementsByTagName("span")[0].innerText = quotation.quotation;
	document.getElementById("author").innerText = quotation.author;
	document.getElementById("work").innerText = quotation.work;
	if (quotation.url != null) {
		document.getElementById("url").href = quotation.url;
	} else {
		document.getElementById("url").removeAttribute("href");
	}
}

function share() {
	if (navigator.share) {
		let quotation = JSON.parse(localStorage.getItem(KEY_QUOTATION));
		let content = "«\u00A0" + quotation.quotation + "\u00A0»";
		if (quotation.author) {
			content += "\n" + quotation.author;
		}
		if (quotation.work) {
			content += "\n" + quotation.work;
		}
		content += "\n\nBel Esprit & Bonne Humeur\n";
		navigator.share({
			title: "Bel Esprit & Bonne Humeur",
			text: content,
			url: ""
		});
	}
}