import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

import org.apache.tools.ant.filters.ReplaceTokens

plugins {
	id 'maven-publish'
}

group = 'company.kano.belespritetbonnehumeur'
version = '3.0'

def date = LocalDateTime.now(ZoneId.of('UTC')).format(DateTimeFormatter.ofPattern('yyyyMMddHHmmss'))
int versionBuild = "git tag --list ${project.name}-${project.version}-* --sort=-version:refname".execute()
		.in.readLines().stream()
		.filter(line -> !line.contains('SNAPSHOT'))
		.findFirst()
		.map(tag -> tag.replaceFirst("^${project.name}-", '').split('-')[1].toInteger()+1)
		.orElse(0)
def versionName = "${version}-${versionBuild}"
if (!project.hasProperty('release')) {
	versionName = "${versionName}-SNAPSHOT"
}

repositories {
	maven {
		url kanoMvnRepoReleases
		credentials {
			username kanoMvnRepoUser
			password kanoMvnRepoPassword
		}
		mavenContent {
			releasesOnly()
		}
		content {
			includeGroupByRegex "company\\.kano.*"
		}
	}
	maven {
		url kanoMvnRepoSnapshots
		credentials {
			username kanoMvnRepoUser
			password kanoMvnRepoPassword
		}
		mavenContent {
			snapshotsOnly()
		}
		content {
			includeGroupByRegex "company\\.kano.*"
		}
	}
	maven {
		url 'https://gitlab.com/api/v4/groups/kano-company/-/packages/maven'
		mavenContent {
			releasesOnly()
		}
		content {
			includeGroupByRegex "company\\.kano.*"
		}
	}
}

configurations {
	assets
}

dependencies {
	assets 'company.kano.belespritetbonnehumeur:belespritetbonnehumeur_data:11.0-0:data@tar.bz2'
}

task clean (type: Delete) {
	delete buildDir
}

task unpackExternalAssets (type: Copy) {
	description 'Unpack external assets.'

	def assetsDir = "${buildDir}/www/data"
	inputs.files(configurations.assets.files)
	outputs.dir(assetsDir)

	from configurations.assets.files.collect { file -> tarTree(file) }
	into assetsDir
}

task copyWww (type: Copy) {
	description 'Copy www content.'

	def inputDir = 'src/main/www'
	def wwwDir = "${buildDir}/www/content"
	inputs.files(project.fileTree(dir: inputDir))
	inputs.property('versionName', versionName)
	outputs.dir(wwwDir)

	from(inputDir) {
		filter(ReplaceTokens, beginToken: '@', endToken: '@', tokens: [version: versionName.toString()])
	}
	into wwwDir
}

task svg2png {
	description 'Convert svg to png'

	def svg2pngDir = "${buildDir}/svg2png"
	inputs.files(project.fileTree(dir: 'src/main/www/images'))
	outputs.dir(svg2pngDir)

	doLast {

		delete svg2pngDir
		mkdir "${svg2pngDir}"

		// icon launcher
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "36x36", "-gravity", "center", "-extent", "36x36", "${svg2pngDir}/belespritetbonnehumeur_36x36.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "48x48", "-gravity", "center", "-extent", "48x48", "${svg2pngDir}/belespritetbonnehumeur_48x48.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "72x72", "-gravity", "center", "-extent", "72x72", "${svg2pngDir}/belespritetbonnehumeur_72x72.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "96x96", "-gravity", "center", "-extent", "96x96", "${svg2pngDir}/belespritetbonnehumeur_96x96.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "144x144", "-gravity", "center", "-extent", "144x144", "${svg2pngDir}/belespritetbonnehumeur_144x144.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "192x192", "-gravity", "center", "-extent", "192x192", "${svg2pngDir}/belespritetbonnehumeur_192x192.png" }
		exec { commandLine "convert", "-background", "none", "src/main/www/images/belespritetbonnehumeur.svg", "-resize", "512x512", "-gravity", "center", "-extent", "512x512", "${svg2pngDir}/belespritetbonnehumeur_512x512.png" }

	}
}

task copyPng (type: Copy, dependsOn: [svg2png, copyWww]) {
	description 'Copy png.'

	def inputDir = "${buildDir}/svg2png"
	def imgDir = "${buildDir}/www/content/images"
	inputs.files(project.fileTree(dir: inputDir))
	outputs.dir(imgDir)

	from inputDir
	into imgDir
}

task assemble (dependsOn: [copyWww, copyPng, unpackExternalAssets]) {
	description 'Assemble web application.'

	outputs.dir("${buildDir}/www")
}

task 'package' (type: Tar) {
	description 'Package data.'

	def outputsDir = "${buildDir}/outputs/www/"
	inputs.files(assemble)
	outputs.dir(outputsDir)

	archiveFileName = 'belespritetbonnehumeur_web.tar.bz2'
	destinationDirectory = file(outputsDir)
	compression = Compression.BZIP2

	from assemble
}

task tag {
	description 'Tag version and push on git.'
	doLast {
		def tag = "${project.name}-${versionName}"
		if (!project.hasProperty('release')) {
			tag += "-${date}"
		}
		exec { commandLine "git", "tag", "${tag}", "-a", "-m", "'tag ${tag}'"}
		exec { commandLine "git", "push", "origin", "${tag}"}
	}
}

task publishAndTag (dependsOn: [publish, tag]) {
	description 'Publish version in repository and tag it on git.'
}

project.afterEvaluate {
	publishing {
		publications {
			maven(MavenPublication) {
				version versionName
				artifact(tasks.getByPath('package')) {
					extension 'tar.bz2'
					builtBy tasks.getByPath('package')
				}
				pom {
					name = 'Bel Esprit & Bonne Humeur'
					description = 'Application Web qui diffuse des citations aléatoirement.'
					url = 'https://www.belespritetbonnehumeur.fr/'
					inceptionYear = '2022'
					organization {
						name = 'Kano'
						url = 'https://www.kano.company/'
					}
					developers {
						developer {
							id = 'Ptitg'
							url = 'https://www.kano.company/'
						}
					}
					licenses {
						license {
							name = 'Apache License, Version 2.0'
							url = 'https://www.apache.org/licenses/LICENSE-2.0'
						}
					}
				}
			}
		}

		repositories {
			maven {
				url project.hasProperty('release') ? kanoMvnRepoReleases : kanoMvnRepoSnapshots
				credentials {
					username kanoMvnRepoUser
					password kanoMvnRepoPassword
				}
			}
		}
	}
}
